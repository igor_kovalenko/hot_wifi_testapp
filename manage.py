# -*- coding: utf-8 -*-

import os
from os.path import dirname, join, normpath, abspath
import inspect
import re
import sys
from flask.cli import main as flask_main
from alembic.config import main as alembic_main
from importlib import import_module
import unittest

BASE_DIR = './hot_wifi_testapp/'
os.environ.setdefault('FLASK_SETTINGS', '{0}.settings'.format(normpath(BASE_DIR)))

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])

    if len(sys.argv) > 1 and sys.argv[1] == 'db':
        sys.argv[1] = '--config={0}'.format(join(abspath(BASE_DIR), 'alembic.ini'))
        sys.exit(alembic_main())

    if len(sys.argv) > 1 and sys.argv[1] == 'test':
        os.environ.setdefault("TESTING", str(True))
        tests = unittest.TestLoader().discover(
            BASE_DIR, pattern='test*.py')
        result = unittest.TextTestRunner(verbosity=2).run(tests)
        if result.wasSuccessful():
            sys.exit(0)
        sys.exit(1)

    flask_app_module_path = dirname(inspect.getabsfile(import_module('flask_app')))
    os.environ.setdefault('FLASK_APP', join(flask_app_module_path, 'app.py'))
    sys.exit(flask_main())
