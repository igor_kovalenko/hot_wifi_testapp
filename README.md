# Тестовое задание компании "Hot WiFi"

## Техническое задание

[Смотрим тут](CONDITIONS.md)

## Установка и конфигурация

+ Забираем проект из репозитария


```
#!bash
    git clone https://igor_kovalenko@bitbucket.org/igor_kovalenko/hot_wifi_testapp.git
    cd hot_wifi_testapp/
```


+ Создаем и активируем виртуальное окружение (песочницу)


```
#!bash
    virtualenv env
    source env/bin/activate
```


+ Устанавливаем зависимости


```
#!bash
    pip install -r requirement.txt
```


+ Тестируем проект (опционально)


```
#!bash
    python manage.py test
```


+ Настраиваем проект


```
#!bash
    python manage.py db upgrade head  
    python manage.py create_superuser
``` 
    

+ Запускаем проект


```
#!bash
    python manage.py run
```


+ Открываем свой любимый брузер и переходим в админку проекта по адресу http://127.0.0.1:5000/admin/ , создаем роль root и включаем в нее суперпользователя, которого создали ранее



+ Авто-документацию API и примечание к его реализации можно посмотреть по адресу http://127.0.0.1:5000/doc/ 



Enjoy! :sparkles:
