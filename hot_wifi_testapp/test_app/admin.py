from .models import Token, PasswordPolicy
from flask_app import admin, AdminModelView, db


admin.add_view(AdminModelView(Token, db.session))
admin.add_view(AdminModelView(PasswordPolicy, db.session))
