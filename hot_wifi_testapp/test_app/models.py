# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from flask_app.database import Base
import datetime


class Token(Base):
    """
    Token model для хранения токенов JWT
    """
    __tablename__ = 'tokens'
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True, autoincrement=True)

    # Токен
    token = Column(String(500), unique=True, nullable=False)

    #: Дата/время создания
    creation_date = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)

    def __init__(self, **kwargs):
        super(Token, self).__init__(**kwargs)

    def __repr__(self):
        return '<id: token: {}'.format(self.token)


class PasswordPolicy(Base):
    """
    PasswordPolicy model для хранения политик паролей
    """
    __tablename__ = 'password_policy'
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True, autoincrement=True)

    # Минимальная длина пароля
    min_length = Column(Integer)

    # Пароль должен содержать цифры?
    has_numbers = Column(Boolean)

    # Пароль должен содержать прописные символы?
    has_uppercase_letters = Column(Boolean)

    # Пароль должен содержать строчные символы?
    has_lowercase_letters = Column(Boolean)

    # Пароль должен содержать специальные символы?
    has_special_symbols = Column(Boolean)

    #: Дата/время создания политики
    creation_date = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)

    def __init__(self, **kwargs):
        super(PasswordPolicy, self).__init__(**kwargs)

    def __repr__(self):
        policy = r'(?=^.{{{0},}}$)'.format(self.min_length)
        policy += r'((?=.*\d)|(?=.*\W+))' if self.has_numbers else ''
        policy += r'(?![.\n])' if self.has_special_symbols else ''
        policy += r'(?=.*[A-Z])' if self.has_uppercase_letters else ''
        policy += r'(?=.*[a-z])' if self.has_lowercase_letters else ''
        policy += r'.*$'

        return policy
