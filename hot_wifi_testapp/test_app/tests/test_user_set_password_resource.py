from flask_app.tests import BaseTestCase
from flask_security.utils import hash_password
import json
from ..jwt_utils import encode_auth_token


class TestUserSetPasswordResource(BaseTestCase):
    """
    Тест ресурса UserSetPasswordResource
    """

    root_username = 'root'
    root_password = 'Qwerty#123'
    root_email = 'root@localhost'
    root_token = None
    root = None

    def setUp(self):
        super().setUp()

        # Создаем суперпользователя
        superuser_role = self._app.user_datastore.find_or_create_role('superuser')

        self.root = self._app.user_datastore.create_user(
            username=self.root_username,
            email=self.root_email,
            password=hash_password(self.root_password)
        )
        self._app.user_datastore.add_role_to_user(self.root, superuser_role)
        self._app.db.session.commit()

        # Создаем токен доступа
        self.root_token = encode_auth_token(self.root_username, self.root_password)

    def test_set_password(self):
        """
        Тестируем установку нового пароля
        """

        # Создаем пользователя без пароля
        user = self._app.user_datastore.create_user(
            username='user',
            email='user@localhost',
            # password=hash_password('rF1234-567!')
        )
        self._app.db.session.commit()

        with self.client:
            # Пытаемся выполнить операцию без токена; Ждем отказ с кодом 401
            response = self.client.put(
                '/api/accounts/{0}/password/'.format(user.id),
                content_type='application/json',
                data=json.dumps({'oldPassword': '', 'newPassword': 'rF1234-567!'}),
            )
            self.assertEqual(response.status_code, 401)

            # Пытаемся выполнить операцию с токеном, но без включения в роль root;
            # Ждем отказ с кодом 401
            response = self.client.put(
                '/api/accounts/{0}/password/'.format(user.id),
                content_type='application/json',
                data=json.dumps({'oldPassword': '', 'newPassword': 'rF1234-567!'}),
                headers={'Authorization': self.root_token})
            self.assertEqual(response.status_code, 401)

            # Включаем учетную запись root в роль root
            root_role = self._app.user_datastore.find_or_create_role('root')
            self._app.user_datastore.add_role_to_user(self.root, root_role)
            self._app.db.session.commit()

            response = self.client.put(
                '/api/accounts/{0}/password/'.format(user.id),
                content_type='application/json',
                data=json.dumps({'oldPassword': '', 'newPassword': 'rF1234-567!'}),
                headers={'Authorization': self.root_token})
            self.assertEqual(response.status_code, 201)
            self.assertTrue(response.content_type == 'application/json')

            response = self.client.put(
                '/api/accounts/{0}/password/'.format(user.id),
                content_type='application/json',
                data=json.dumps({'oldPassword': 'rF1234-567!', 'newPassword': '123'}),
                headers={'Authorization': self.root_token})
            self.assertEqual(response.status_code, 400)
