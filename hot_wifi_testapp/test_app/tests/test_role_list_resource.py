from flask_app.tests import BaseTestCase
from flask_security.utils import hash_password
import json
from ..jwt_utils import encode_auth_token


class TestRoleListResource(BaseTestCase):
    """
    Тест ресурса RoleListResource
    """

    root_username = 'root'
    root_password = 'Qwerty#123'
    root_email = 'root@localhost'
    root_token = None
    root = None

    def setUp(self):
        super().setUp()

        # Создаем суперпользователя
        superuser_role = self._app.user_datastore.find_or_create_role('superuser')

        self.root = self._app.user_datastore.create_user(
            username=self.root_username,
            email=self.root_email,
            password=hash_password(self.root_password)
        )
        self._app.user_datastore.add_role_to_user(self.root, superuser_role)
        self._app.db.session.commit()

        # Создаем токен доступа
        self.root_token = encode_auth_token(self.root_username, self.root_password)

    def test_role_user_list(self):
        """
        Тестируем на возврат списка ролей
        """

        with self.client:
            # Пытаемся получить доступ к ресурсу без токена; Ждем отказ с кодом 401
            response = self.client.get('/api/roles/', content_type='application/json')
            self.assertEqual(response.status_code, 401)

            # Пытаемся получить доступ с токеном, но без включения в роль root;
            # Ждем отказ с кодом 401
            response = self.client.get(
                '/api/roles/',
                content_type='application/json',
                headers={'Authorization': self.root_token})
            self.assertEqual(response.status_code, 401)

            # Включаем учетную запись root в роль root
            root_role = self._app.user_datastore.find_or_create_role('root')
            self._app.user_datastore.add_role_to_user(self.root, root_role)
            self._app.db.session.commit()

            response = self.client.get(
                '/api/roles/',
                content_type='application/json',
                headers={'Authorization': self.root_token})
            self.assertEqual(response.status_code, 200)
            self.assertTrue(response.content_type == 'application/json')
            self.assertEqual(len(json.loads(response.data.decode())), 2)
