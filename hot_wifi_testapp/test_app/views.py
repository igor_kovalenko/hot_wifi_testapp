# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from flask import Blueprint, abort, redirect, url_for
from flask_restplus import Resource, Api, fields
from flask_app.models import User, Role
from flask_app import app
from flask_babelex import gettext as _
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound
from flask_security.utils import verify_password, hash_password
from .jwt_utils import encode_auth_token, auth_token_required, roles_required, delete_active_token
from .models import PasswordPolicy
import re


bp = Blueprint('test_app', __name__, template_folder='templates')

authorizations = {
    'jwt': {
        'type': "apiKey",
        'in': "header",
        'name': "Authorization",
        'description': "JWT authorization"
    }
}

api = Api(bp,
          authorizations=authorizations,
          security='jwt',
          version='0.0.1',
          title='Примечание к реализации API тестового приложения компании Hot-WiFi',
          description="""
* API реализует аутентификацию посредством механизма веб-токенов, и поэтому каждый запрос должен
в заголовке Authorization содержать полученный через ресурс /api/login токен. Этот токен будет
действовать до тех пор, пока не будет отзван через механизм ресурса /api/logout

* API реализует авторизацию посредством ролевого механизма - учетная запись желающая получить доступ
к API должна быть включена в роль root

* При создании нового пользователя пароль ему не будет назначен, - его следует назначить отдельно
Не назначенный (пустой) пароль ресуру /api/accounts/{id}/password/ должен предоставлен как ""

* Сложность пароля определяется политиками. Политик может быть много, но активной считается
одна - последняя. Кроме того, следует иметь ввиду, что если полика не задана, то система создаст ее
самостоятельно используя значения по-умолчанию.
          """, doc='/doc/')

app.config.SWAGGER_UI_DOC_EXPANSION = 'list'
app.config['RESTPLUS_VALIDATE'] = True
app.config['SECURITY_TOKEN_AUTHENTICATION_HEADER'] = 'Authorization'


@bp.route('/')
def show_api_doc():
    return redirect(url_for('test_app.doc'))


role_resource = api.model('Roles', {
    'uri': fields.Url('test_app.role_ep', readonly=True),
    'id': fields.Integer(readonly=True, description=_('The role unique identifier')),
    'name': fields.String(required=True, description=_('Role name')),
    'description': fields.String(description=_('Description')),
})


password_resource = api.model('Password', {
    'oldPassword': fields.String(required=True, description=_('Old password')),
    'newPassword': fields.String(required=True, description=_('New password'))
})

user_resource = api.model('User', {
    'uri': fields.Url('test_app.user_ep', readonly=True),
    'id': fields.Integer(readonly=True, description=_('The user unique identifier')),
    'email': fields.String(required=True, description=_('User email')),
    'username': fields.String(required=True, description=_('User name')),
    'password': fields.String(readonly=True, description=_('Password')),
    'last_login_at': fields.DateTime(readonly=True, description=_('Last login at')),
    'current_login_at': fields.DateTime(readonly=True, description=_('Current login at')),
    'last_login_ip': fields.String(readonly=True, description=_('Last login IP')),
    'current_login_ip': fields.String(readonly=True, description=_('Current login IP')),
    'login_count': fields.Integer(readonly=True, description=_('Login count')),
    'active': fields.Boolean(default=True, description=_('Active')),
    'confirmed_at': fields.DateTime(readonly=True, description=_('Confirmed at')),
    'isExternalAccount': fields.Boolean(attribute='is_external_account',
                                        description=_('Is external account')),
    'roles': fields.Nested(role_resource, readonly=True, as_list=True, skip_none=True)
})

login_resource = api.model('Login', {
    'username': fields.String(required=True, description=_('User name')),
    'password': fields.String(required=True, description=_('Password')),
})

jwt_response_resource = api.model('JWTResponse', {
    'status': fields.String(required=True, description=_('Status')),
    'message': fields.String(required=True, description=_('Message')),
    'auth_token': fields.String(required=True, description=_('Authorization token')),
})

password_policy_resource = api.model('PasswordPolicy', {
    'length': fields.Integer(
        default=8, attribute='min_length', description=_('Min password length')),
    'numbers': fields.Boolean(
        default=True, attribute='has_numbers', description=_('Password has number symbols')),
    'uppercaseLetters': fields.Boolean(
        default=True, attribute='has_uppercase_letters',
        description=_('Password has uppercase symbols')
    ),
    'lowercaseLetters': fields.Boolean(
        default=True, attribute='has_lowercase_letters',
        description=_('Password has lowercase symbols')
    ),
    'specialSymbols': fields.Boolean(
        default=False, attribute='has_special_symbols',
        description=_('Password has special symbols')
    ),
    'creation_date': fields.DateTime(
        readonly=True, attribute='creation_date', description=_('Police creation date and time'))
})


@api.route('/api/accounts/', endpoint='users_ep')
class UserListResource(Resource):
    @roles_required('root')
    @api.marshal_list_with(user_resource)
    @api.response(200, 'Success', [user_resource])
    def get(self):
        """
        Получить список пользователей системы
        """
        return User.query.all()

    @roles_required('root')
    @api.expect(user_resource, validate=True)
    @api.marshal_with(user_resource)
    @api.response(201, 'Success', user_resource)
    @api.response(400, 'Validation Error')
    def post(self):
        """
        Добавить пользователя в систему
        """
        try:
            user_role = app.user_datastore.find_or_create_role('user')
            user = app.user_datastore.create_user(
                username=api.payload['username'],
                email=api.payload['email'],
                is_external_account=api.payload.get('isExternalAccount', False),
                active=api.payload.get('active', True)
            )

            app.user_datastore.add_role_to_user(user, user_role)
            app.db.session.commit()
            app.db.session.refresh(user)

            return user, 201
        except IntegrityError:
            abort(400, 'Validation Error')


@api.route('/api/accounts/<int:id>/', endpoint='user_ep')
@api.doc(params={'id': 'An ID'})
class UserDetailsResource(Resource):

    @roles_required('root')
    @api.marshal_with(user_resource)
    @api.response(200, 'Success', user_resource)
    @api.response(400, 'User not found')
    def get(self, id):
        """
        Получить детальную информацию о пользователе
        """
        try:
            return User.query.filter_by(id=id).one()
        except MultipleResultsFound:
            abort(400)
        except NoResultFound:
            abort(400)

    @roles_required('root')
    @api.response(204, 'Deleted successfully')
    @api.response(400, 'User not found')
    def delete(self, id):
        """
        Удалить учетную запись
        """
        try:
            u = User.query.filter_by(id=id).one()
            app.db.session.delete(u)
            app.db.session.commit()
            return None, 204
        except MultipleResultsFound:
            abort(400)
        except NoResultFound:
            abort(400)


@api.route('/api/accounts/<int:id>/role/<int:role_id>/', endpoint='user_role_ep')
@api.doc(params={'id': 'An ID'})
class UserAddToRoleResource(Resource):

    @roles_required('root')
    @api.marshal_with(user_resource, 201)
    @api.response(201, 'Success', user_resource)
    @api.response(400, 'Bad request')
    def post(self, id, role_id):
        """
        Добавить пользователя в роль
        """
        try:
            user = User.query.filter_by(id=id).one()
            role = Role.query.filter_by(id=role_id).one()

            if not user.has_role(role):
                app.user_datastore.add_role_to_user(user, role)
                app.db.session.commit()
                app.db.session.refresh(user)
                return user, 201
            abort(400, 'Bad request')
        except IntegrityError:
            abort(400, 'Bad request')

    @roles_required('root')
    @api.response(204, 'Deleted successfully')
    @api.response(400, 'Bad request')
    def delete(self, id, role_id):
        """
        Удалить пользователя из роли
        """
        try:
            user = User.query.filter_by(id=id).one()
            role = Role.query.filter_by(id=role_id).one()

            if user.has_role(role):
                app.user_datastore.remove_role_from_user(user, role)
                app.db.session.commit()
                return None, 204
            abort(400, 'Bad request')
        except IntegrityError:
            abort(400, 'Bad request')


@api.route('/api/accounts/<int:id>/password/', endpoint='user_set_password_ep')
@api.doc(params={'id': 'An ID'})
class UserSetPasswordResource(Resource):

    @roles_required('root')
    @api.expect(password_resource, validate=True)
    @api.response(201, 'Success')
    @api.response(400, 'Validation Error')
    @api.response(401, 'Authorization failed')
    def put(self, id):
        """
        Установить новый пароль для учетной записи
        """

        # Получаем политику сложности пароля
        policy = PasswordPolicy.query.order_by(PasswordPolicy.creation_date.desc()).first()
        if not policy:
            # Если политика не установлена, то устанавливаем политику по-умолчанию
            policy = PasswordPolicy(min_length=8, has_numbers=True, has_uppercase_letters=True,
                                    has_lowercase_letters=True, has_special_symbols=False)
            app.db.session.add(policy)
            app.db.session.commit()

        # Проверяем пароль на соответствие политике
        p = re.compile(str(policy))
        if not p.match(api.payload['newPassword']):
            abort(400, _('The complexity of the password does not match the active policy'))

        try:
            u = User.query.filter_by(id=id).one()
            if u.password:
                if not verify_password(
                        password=api.payload['oldPassword'], password_hash=u.password):
                    abort(401)
            u.password = hash_password(api.payload['newPassword'])
            app.db.session.commit()
        except MultipleResultsFound:
            abort(400)
        except NoResultFound:
            abort(400)

        return None, 201


@api.route('/api/roles/', endpoint='roles_ep')
class RoleListResource(Resource):
    @roles_required('root')
    @api.marshal_with(role_resource)
    @api.response(200, 'Success', [role_resource])
    def get(self):
        """
        Получить список пользовательских ролей
        """
        return Role.query.all()


@api.route('/api/roles/<int:id>/', endpoint='role_ep')
@api.doc(params={'id': 'An ID'})
class RoleDetailsResource(Resource):
    @roles_required('root')
    @api.marshal_with(role_resource)
    @api.response(200, 'Success', role_resource)
    @api.response(400, 'Bad request')
    def get(self, id):
        """
        Получить детальную информацию о пользовательской роли
        """
        try:
            return Role.query.filter_by(id=id).one()
        except MultipleResultsFound:
            abort(400)
        except NoResultFound:
            abort(400)


@api.route('/api/login/', endpoint='login_ep')
class LoginResource(Resource):
    """
    Позволяет пользователю системы аутентифицировать себя и получить токен доступа
    """
    @api.doc(security=None)
    @api.expect(login_resource, validate=True)
    @api.marshal_with(jwt_response_resource, 201)
    @api.response(201, 'Success', jwt_response_resource)
    @api.response(400, 'Validation Error')
    @api.response(401, 'Authorization failed')
    def post(self):
        """
        Получить токен доступа к API системы
        """
        token = encode_auth_token(api.payload['username'], api.payload['password'])

        if token:
            return {
                'status': 'Success',
                'message': 'Success login',
                'auth_token': token
            }, 201

        abort(401)


@api.route('/api/logout/', endpoint='logout_ep')
class LogoutResource(Resource):
    """
    Позволяет пользователю завершить сеанс работы с системой
    """
    @auth_token_required
    @api.response(201, 'Success')
    @api.response(401, 'Authorization failed')
    def post(self):
        """
        Завершить сеанс работы с системой
        """
        delete_active_token()
        return None, 201


@api.route('/api/accounts/password/policy/', endpoint='password_policy_ep')
class PasswordPolicyResource(Resource):
    """
    Устанавливает политику сложности пароля
    """
    @roles_required('root')
    @api.expect(password_policy_resource, validate=True)
    @api.response(201, 'Success')
    @api.response(400, 'Validation Error')
    def post(self):
        """
        Установить политику сложности пароля
        """
        try:
            policy = PasswordPolicy(
                min_length=api.payload.get('length', 8),
                has_numbers=api.payload.get('numbers', True),
                has_uppercase_letters=api.payload.get('uppercaseLetters', True),
                has_lowercase_letters=api.payload.get('lowercaseLetters', True),
                has_special_symbols=api.payload.get('specialSymbols', False),
            )
            app.db.session.add(policy)
            app.db.session.commit()

            return str(policy), 201
        except IntegrityError:
            abort(400, 'Validation Error')
