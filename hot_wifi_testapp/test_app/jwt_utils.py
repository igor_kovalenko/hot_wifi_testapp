# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import jwt
import datetime
from functools import wraps
from flask import request, abort
from flask_app import app
from flask_app.models import User
from .models import Token
from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound
from flask_security.utils import verify_password


def encode_auth_token(username, password):
    """
    Generates the Auth Token
    :return: string
    """
    try:
        u = User.query.filter_by(username=username).one()
        if u.password and verify_password(password=password, password_hash=u.password):
            try:
                payload = {
                    # 'exp': datetime.datetime.utcnow() + datetime.timedelta(days=0, seconds=5),
                    'iat': datetime.datetime.utcnow(),
                    'sub': u.id
                }
                token = jwt.encode(
                    payload,
                    app.config['SECRET_KEY'],
                    algorithm='HS256'
                )

                # Store token in base
                t = Token(token=token.decode('utf-8'))
                app.db.session.add(t)
                app.db.session.commit()

                return token.decode('utf-8')
            except Exception as e:
                print(str(e))
                return None
    except MultipleResultsFound:
        return None
    except NoResultFound:
        return None


def decode_auth_token(auth_token, secret_key):
    """
    Decodes the auth token
    :param auth_token:
    :return: integer|string
    """
    try:
        payload = jwt.decode(auth_token, secret_key)
        return payload['sub']
    except jwt.ExpiredSignatureError:
        return 'Signature expired. Please log in again.'
    except jwt.InvalidTokenError:
        return 'Invalid token. Please log in again.'


def delete_active_token():
    try:
        t = Token.query.filter_by(token=request.headers['Authorization']).one()
        app.db.session.delete(t)
        app.db.session.commit()
    except MultipleResultsFound:
        abort(401)
    except NoResultFound:
        abort(401)


def auth_token_required(fn):
    @wraps(fn)
    def check_token(*args, **kwargs):
        try:
            t = Token.query.filter_by(token=request.headers['Authorization']).one()
            payload = jwt.decode(t.token, app.config['SECRET_KEY'])
            User.query.filter_by(id=payload['sub']).one()
            return fn(*args, **kwargs)
        except MultipleResultsFound:
            abort(401)
        except NoResultFound:
            abort(401)
        except jwt.ExpiredSignatureError:
            return abort(401)
        except jwt.InvalidTokenError:
            return abort(401)

        return abort(401)

    return check_token


def has_roles(u, *roles):
    for role in roles:
        if u.has_role(role):
            continue
        return False
    return True


def roles_required(*roles):
    def wrapper(fn):
        @wraps(fn)
        def check_token(*args, **kwargs):
            if 'Authorization' not in request.headers:
                return abort(401)
            try:
                t = Token.query.filter_by(token=request.headers['Authorization']).one()
                payload = jwt.decode(t.token, app.config['SECRET_KEY'])
                u = User.query.filter_by(id=payload['sub']).one()
                if u and has_roles(u, *roles):
                    return fn(*args, **kwargs)
            except MultipleResultsFound:
                abort(401)
            except NoResultFound:
                abort(401)
            except jwt.ExpiredSignatureError:
                return abort(401)
            except jwt.InvalidTokenError:
                return abort(401)

            return abort(401)

        return check_token
    return wrapper
